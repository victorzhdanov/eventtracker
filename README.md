# README #

This application is a personal anniversary tracker with Swing GUI. 

You can add events, such as birthdays, wedding days, name days etc., and easily track key information about it: which anniversary will be next (and which ones will be after it), which person is related to this annioversary, how many days left before, etc.

All user data is stored in the same folder, where jar is located. You can chose plain text format, serialized objects (takes a bit more disk space), or XML (coming soon).

Here is main window of EventTracker:

![MainWindow.png](https://bitbucket.org/repo/94keK7/images/546657689-MainWindow.png)

You need [JRE8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) to run this app.