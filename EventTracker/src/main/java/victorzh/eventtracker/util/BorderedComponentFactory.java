package victorzh.eventtracker.util;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

/**
 * This class with static methods helps make code of frames and dialogs
 * shorter by providing ready-for-use component with titled border in one line
 * of code.
 * 
 * @author Victor Zhdanov
 *
 */

public class BorderedComponentFactory {
	
	public static JPanel getTitledPanel(final String title) {
		final JPanel panel = new JPanel();
		panel.setBorder(makeBorder(title));
		return panel;
	}

	public static JTextField getTitledTextField(final String title) {
		final JTextField textField = new JTextField();
		textField.setBorder(makeBorder(title));
		return textField;
	}

	private static TitledBorder makeBorder(final String title) {
		final TitledBorder border = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), title);
		return border;
	}
	
}
