package victorzh.eventtracker.util;

import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;

/**
 * This class performs calculation of the needed location of window on user's
 * screen, considering the size of the window (passed as an argument to the 
 * suitable method), and returns an instance of class {@link java.awt.Point}
 * with position of desired location.
 * <p>
 * The instance of class {@link java.awt.Point} can be obtained by calling one
 * of the next methods:
 * {@link #getCenteredPosition(Window)}
 * {@link #getTopLeftPosition()}
 * and used to be passed to {@link java.awt.Window#setLocation(Point)} method, 
 * that helps correctly locate window in the center of user's screen, 
 * regardless of it's dimension.
 * <p>
 * If window is null, <code>Point(0, 0)</code> will be returned.
 * 
 * @author Victor Zhdanov
 */

public class WindowPositionAdjuster {
	
	/**
	 * @param window Parent window instance
	 * @return Centered location for the screen
	 */
	public static Point getCenteredPosition(final Window window) {
		final int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
		final int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
		
		if (window == null)
			return new Point(0, 0);
		
		else return new Point(
					(screenWidth / 2) - (window.getSize().width / 2), 
					(screenHeight / 2) - (window.getSize().height / 2));
		
		
	}
	
	/**
	 * @return Top left corner location of the screen (0, 0 location)
	 */
	public static Point getTopLeftPosition() {
		return new Point(0, 0);
	}

}
