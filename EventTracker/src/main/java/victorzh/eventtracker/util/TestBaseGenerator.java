package victorzh.eventtracker.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Test base generator for load testing.
 * Part of 'developer's mode' in future (see TO-DO's 'IN NEXT MAJOR RELEASE'
 * section).
 * <p>
 * Please specify 
 * <code>victorzh.eventtracker.util.TestBaseGenerator.OUTPUT_FILE</code>
 * manually!
 * 
 * @author Victor Zhdanov
 *
 */

public class TestBaseGenerator {
	private static final String OUTPUT_FILE = "E:/Dev/etbase/testBase.dat";
	private static final String TEMPLATE = "2000-01-01|Test entry N %d|LastName&FirstName&MiddleName|false|true|false";
	
	/**
	 * Generates given amount of test events, matching with string template.
	 * 
	 * @param events Amount of test events in base
	 */
	public static void generateBase(final int events) {
		try {
			System.out.println("Start generating test base...");
			
			final PrintWriter writer = new PrintWriter(new FileOutputStream(new File(OUTPUT_FILE)), true);
			
			long startTime = System.currentTimeMillis();
			
			int eventCounter = 0;
			
			for (eventCounter = 1; eventCounter <= events; eventCounter++) {
				writer.printf(TEMPLATE, eventCounter);
				writer.println();
			}
			
			long endTime = System.currentTimeMillis() - startTime;

			writer.close();
			
			System.out.println(
					"Test base generated. " + (eventCounter - 1) 
					+ " events added in " + endTime + " ms.");
		
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	public static void main(String[] args) {
		TestBaseGenerator.generateBase(100);
	}
}
