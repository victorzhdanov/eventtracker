package victorzh.eventtracker.util;

import java.time.DateTimeException;
import java.time.LocalDate;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * This class is a simple Swing GUI wrapper for mechanism of setting up date
 * in <code>java.time.LocalDate</code>.
 * <p>
 * While initialization of this class instance via default constructor,
 * current system date is set as component's date value.
 * <p>
 * Note, that no control for selected date correctness is carried. No leap
 * year control, no 30/31 days in month check. This component is just a
 * selector. Take care of date control in your code, while using dates,
 * provided by this component.
 * 
 * @author Victor Zhdanov
 * 
 */

@SuppressWarnings("serial")
public class DatePicker extends JPanel {
	private JComboBox<Integer> yearPicker;
	private JComboBox<Integer> monthPicker;
	private JComboBox<Integer> dayPicker;
	private final JPanel comboPanel;
	
	
	public DatePicker() {
		this.comboPanel = BorderedComponentFactory.getTitledPanel("Event date");
		setPickerRange();		
		setDate(LocalDate.now());
		
		this.comboPanel.add(dayPicker);
		this.comboPanel.add(monthPicker);
		this.comboPanel.add(yearPicker);
		add(this.comboPanel);
	}
	
	
	/**
	 * 
	 * @param date LocalDate
	 * @return Instance of this component, where date selector positions match
	 * with LocalDate, passed to this method as parameter.
	 */
	public DatePicker setDate(final LocalDate date) {
		this.yearPicker.setSelectedItem(date.getYear());
		this.monthPicker.setSelectedItem(date.getMonthValue());
		this.dayPicker.setSelectedItem(date.getDayOfMonth());
		return this;
	}
	
	/**
	 * 
	 * @return Date, selected in this component
	 * @throws DateTimeException
	 */
	public LocalDate getSelectedDate() throws DateTimeException {
		final int year = (int)yearPicker.getSelectedItem();
		final int month = (int)monthPicker.getSelectedItem();
		final int day = (int)dayPicker.getSelectedItem();
		return LocalDate.of(year, month, day);
	}
	
	/**
	 * Dates range can be set from 01.01.1900 to 31.12.2099.
	 * Note, that every month (including February) can be matched with 31th
	 * day of month.
	 * 
	 */
	private void setPickerRange() {
		
		//Setting year range
		final DefaultComboBoxModel<Integer> yearModel = new DefaultComboBoxModel<>();
		for (int i = 1900; i <= 2099; i++)
			yearModel.addElement(Integer.valueOf(i));

		this.yearPicker = new JComboBox<Integer>();
		this.yearPicker.setModel(yearModel);
		
		//Setting month range
		final DefaultComboBoxModel<Integer> monthModel = new DefaultComboBoxModel<>();
		for (int i = 1; i <= 12; i++)
			monthModel.addElement(Integer.valueOf(i));
		
		this.monthPicker = new JComboBox<Integer>();
		this.monthPicker.setModel(monthModel);
		
		//Setting day range
		final DefaultComboBoxModel<Integer> dayModel = new DefaultComboBoxModel<>();
		for (int i = 1; i<= 31; i++)
			dayModel.addElement(Integer.valueOf(i));
		
		this.dayPicker = new JComboBox<Integer>();
		this.dayPicker.setModel(dayModel);
	}
}
