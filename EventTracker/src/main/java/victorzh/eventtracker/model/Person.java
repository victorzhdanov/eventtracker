package victorzh.eventtracker.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * This class describes person, related to event.
 * Person has no birth- or any other type of date, because in the context
 * of current program, person is nothing more than one of fields, describes an
 * event, located in <code>victorzh.eventtracker.model.AnniversaryEvent</code>
 * <p>
 * This class is immutable.
 *
 * @param names Last, first and middle name of person (order matters)
 * 
 * @author Victor Zhdanov
 *
 */

public class Person implements Serializable {
	private static final long serialVersionUID = 4527157988200039358L;
	
	//names[0] = last name; names[1] = first name; names[2] = middle name
	private String[] names = {"", "", ""};
	
	public Person(final String[] names) {
		for (int i = 0; i < names.length; i++)
			if (!names[i].equals(""))
				this.names[i] = names[i];
	}
	
	public String getLastName() {
		return this.names[0];
	}
	
	public String getFirstName() {
		return this.names[1];
	}
	
	public String getMiddleName() {
		return this.names[2];
	}
		
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.names[0]);
		if (!this.names[0].equals(""))
			sb.append(" ");
		sb.append(this.names[1]);
		if (!this.names[1].equals(""))
			sb.append(" ");
		sb.append(this.names[2]);
		
		return sb.toString();
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.names[0], this.names[1], this.names[2]);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj.getClass() != (this.getClass()))
			return false;
		else {
			Person p = (Person)obj;
			
			return
					this.names[0].equals(p.getLastName())
					&& this.names[1].equals(p.getFirstName())
					&& this.names[2].equals(p.getMiddleName());
		}
	}

}
