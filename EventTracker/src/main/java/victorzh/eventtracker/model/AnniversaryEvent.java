package victorzh.eventtracker.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class encapsulates core information about event.
 * It is a part of Model in MVC application structure.
 * While 'empty' events, that does not contain any useful information,
 * are undesirable, constructor without parameters is not available.
 * The only available constructor obtains the following information:
 * 
 * <ul>
 * <li>Date of the event;</li>
 * <li>Description of the event;</li>
 * <li>Person, related with the event;</li>
 * <li>Reminder state;</li>
 * <li>Repeat frequency.</li>
 * </ul>
 * 
 * More information about parameters is available in constructor specification.
 * 
 * @author Victor Zhdanov
 *
 */

public class AnniversaryEvent implements Comparable<AnniversaryEvent>, Serializable {
	private static final long serialVersionUID = 2442426454638111092L;
	private LocalDate eventDate;	
	private String description;
	private List<String> previousDescriptions;
	private Person person;
	private boolean reminder;
	private boolean yearlyRepeat;
	private boolean monthlyRepeat;
	
	
	/**
	 * Constructor
	 * 
	 * @param year	Year of the event (for eventDate field)
	 * 
	 * @param month	Month of the event (for eventDate field)
	 * 
	 * @param day	Day of the event (for eventDate field)
	 * 
	 * @param description	Description of the event (any text from user input)
	 * 
	 * @param person	Person, related with the event
	 * 
	 * @param reminder	Reminder state. True, if reminder is on, false if off.
	 * On by default. 
	 * 
	 * @param repeat	Repeater state. Indicates, how often the event will be
	 * reminded about (monthly/yearly)
	 */
	
	public AnniversaryEvent(
			final LocalDate date, 
			final String description, 
			final Person person, 
			final boolean reminder, 
			final boolean yearlyRepeat, 
			final boolean monthlyRepeat) {
		
		this.eventDate = date;
		this.description = description;
		this.person = person;
		this.reminder = reminder;
		this.yearlyRepeat = yearlyRepeat;
		this.monthlyRepeat = monthlyRepeat;
	}
	
	public LocalDate getDate() {
		return this.eventDate;
	}
	
	public Person getPerson() {
		return this.person;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public boolean isReminderOn() {
		return this.reminder;
	}
	
	public boolean isMonthlyRepeatOn() {
		return this.monthlyRepeat;
	}
	
	public boolean isYearlyRepeatOn() {
		return this.yearlyRepeat;
	}
	
	public void changeDescription(final String newDescription) {
		if (this.previousDescriptions == null)
			this.previousDescriptions = new ArrayList<String>();
			
		this.previousDescriptions.add(this.description);
		this.description = newDescription;
	}
	
	public void changePersonDetails(final Person person) {
		if (!this.person.equals(person))
			this.person = person;
	}
	
	public void changeYearlyRepeat(final boolean repeat) {
		this.yearlyRepeat = repeat;
	}
	
	public void changeMonthlyRepeat(final boolean repeat) {
		this.monthlyRepeat = repeat;
	}
	
	public void changeReminderState(final boolean state) {
		if (this.reminder != state)
			this.reminder = state;
	}
	
	public List<String> getDescriptionHistory() {
		if (this.previousDescriptions != null)
			return this.previousDescriptions;
		else {
			final List<String> d = new ArrayList<>();
			d.add("no previous descriptions");
			return d;
		}
	}
	
	@Override
	public String toString() {
		return 
				this.eventDate.toString()
				+ "|"
				+ this.description
				+ "|"
				+ this.person
				+ "|"
				+ this.reminder
				+ "|"
				+ this.yearlyRepeat
				+ "|"
				+ this.monthlyRepeat;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.eventDate, this.description, this.person);
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (this.getClass() != obj.getClass())
			return false;
		else {
			final AnniversaryEvent other = (AnniversaryEvent) obj;
			return this.eventDate.equals(other.eventDate) 
					&& this.description.equals(other.description) 
					&& this.person.equals(other.person)
					&& this.reminder == other.reminder
					&& this.yearlyRepeat == other.yearlyRepeat
					&& this.monthlyRepeat == other.monthlyRepeat;
		}
	}

	@Override
	public int compareTo(final AnniversaryEvent event) {
		if (this.getDate().isBefore(event.getDate()))
			return -1;
		if (this.getDate().isAfter(event.getDate()))
			return 1;
		else 
			return 0;
	}

}
