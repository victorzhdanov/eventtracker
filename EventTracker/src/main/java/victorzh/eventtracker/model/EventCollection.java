package victorzh.eventtracker.model;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import victorzh.eventtracker.services.DataLoadMode;
import victorzh.eventtracker.services.FileLoader;

/**
 * This class wraps collections of events and anniversaries, (both are created
 * by methods of this class), and provides CRUD operations to manage it.
 * <p>
 * User data can be stored in file as plain text or as serialized object.
 * The concrete form of stored data can be selected in program settings.
 * Serialized object is selected by default.
 * 
 * @author Victor Zhdanov
 *
 */

public class EventCollection {
	//While global settings are not implemented, file names are temporary placed here
	public static final String EVENTS_PLAIN = "events.dat";
	public static final String EVENTS_SERIALIZED = "events.ser";
	public static final String ANNIVERSARIES_PLAIN = "anniversaries.dat";
	public static final String ANNIVERSARIES_SERIALIZED = "anniversaries.ser";
	
	private static final int DECEMBER = 12;
	
	private final LocalDate today;
	private final List<AnniversaryEvent> eventList;
	private final List<Anniversary> anniversaryList;
	
	private Path eventBaseFile;
	private Path anniversaryBaseFile;
	private Path serializedEventsPath;
	private Path serializedAnniversariesPath;
	
	private DataLoadMode mainLoadMode;
	private DataLoadMode testLoadMode;
	
	private final Logger log = LoggerFactory.getLogger(EventCollection.class);
	
	/**
	 * Main constructor, used while program is running.
	 */
	public EventCollection() {
		this.today = LocalDate.now();
		this.eventList = new ArrayList<>();
		this.anniversaryList = new ArrayList<>();
		
		//Text base files
		this.eventBaseFile = Paths.get(EVENTS_PLAIN);
		this.anniversaryBaseFile = Paths.get(ANNIVERSARIES_PLAIN);
		
		//Serialized object files
		this.serializedEventsPath = Paths.get(EVENTS_SERIALIZED);
		this.serializedAnniversariesPath = Paths.get(ANNIVERSARIES_SERIALIZED);
		
//		fillEventList(this.eventBaseFile);
		fillSerializedEventList(this.serializedEventsPath);
	}
	
	/**
	 * Constructor with argument is used while testing this class.
	 * 
	 * @param path File with events
	 */
	public EventCollection(final Path path) {
		this.today = LocalDate.now();
		this.eventList = new ArrayList<>();
		this.anniversaryList = new ArrayList<>();
		
		//Text base files
		this.eventBaseFile = path;
		this.anniversaryBaseFile = Paths.get(ANNIVERSARIES_PLAIN);
		
		//Serialized object files
		this.serializedEventsPath = Paths.get(EVENTS_SERIALIZED);
		this.serializedAnniversariesPath = Paths.get(ANNIVERSARIES_SERIALIZED);
		
		fillEventList(this.eventBaseFile);
//		fillSerializedEventList(this.serializedEventsPath);
	}
	
	public void setMainLoadMode(final DataLoadMode mode) {
		this.mainLoadMode = mode;
	}
	
	public void setTestLoadMode(final DataLoadMode mode) {
		this.testLoadMode = mode;
	}
	
	/**
	 * This method get list of events, converted from file in 
	 * <code>victorzh.eventtracker.services.FileLoader</code>, and
	 * add it to <code>List&lt;AnniversaryEvent&gt; eventList</code>,
	 * than used to be a source for AnniversaryList fill method.
	 * 
	 * @param path Path to file to load data from
	 */
	public void fillEventList(final Path path) {
		try {
			this.eventList.clear();
			this.eventList.addAll(FileLoader.loadEvents(path));
			this.anniversaryList.clear();
			addAnniversariesToList(this.eventList);
			log.info("Events successfully loaded from file {}.", this.eventBaseFile);
		} catch (IOException e) {
			log.error("Error loading events from file in fillEventList. {} thrown.", e.getClass());
		}
	}
	
	/**
	 * 
	 * @param path Path to file, from witch serialized events will be loaded 
	 */
	public void fillSerializedEventList(final Path path) {
		try {
			this.eventList.clear();
			this.eventList.addAll(FileLoader.readSerializedEvents(path));
			this.anniversaryList.clear();
			addAnniversariesToList(this.eventList);
			log.info("Serialized event successfully loaded from file {}.", this.serializedEventsPath);
		}
		catch (IOException e) {
			log.error(
					"Error loading serialized events. {} thrown, message is: {}", 
					e.getClass(), 
					e.getMessage());
		}
	}
	
	/**
	 * Set new path to file, in which event base is stored. 
	 * 
	 * @param eventBaseFile New event base file path
	 */
	public void setEventFilePath(final Path filePath) {	
		if (FileLoader.isFileValidAsBase(filePath))
			this.eventBaseFile = filePath;
	}
	
	/**
	 * 
	 * @return Path to file, in which event base is stored.
	 */
	public Path getEventFilePath() {
		return this.eventBaseFile;
	}
	
	/**
	 * 
	 * @param filePath New anniversary base file path
	 */
	public void setAnniversaryFilePath(final Path filePath) {
		if (FileLoader.isFileValidAsBase(filePath))
			this.anniversaryBaseFile = filePath;
	}
	
	/**
	 * 
	 * @return Path to file, in which anniversaries are stored (used by
	 * <code>victorzh.eventtracker.services.Reminder</code>.
	 */
	public Path getAnniversaryFilePath() {
		return this.anniversaryBaseFile;
	}
	
	/**
	 * 
	 * @return Path to file with serialized event objects
	 */
	public Path getSerializedEventsPath() {
		return this.serializedEventsPath;
	}
	
	/**
	 * 
	 * @return Path to file with serialized anniversary objects
	 */
	public Path getSerializedAnniversariesPath() {
		return this.serializedAnniversariesPath;
	}
	

	/**
	 * @return Quantity of events in list
	 */
	public int getEventListSize() {
		return this.eventList.size();
	}
	
	/**
	 * 
	 * @return Quantity of anniversaries in list (for adding in event container)
	 */
	public int getAnniversaryListSize() {
		return this.anniversaryList.size();
	}
	
	/**
	 * 
	 * @param event New event
	 */
	public void addEvent(final AnniversaryEvent event) throws DateTimeException { 
		this.eventList.add(event);
		addNewAnniversaryToList(event);
	}

	/**
	 * 
	 * @return List of events
	 */
	public List<AnniversaryEvent> getEventList() { 
		return this.eventList;
	}
	
	/**
	 * 
	 * @return List of anniversaries (for adding in event container)
	 */
	public List<Anniversary> getAnniversaryList() {
		return this.anniversaryList;
	}
	
	/**
	 * 
	 * @param event Event to be edited
	 */
	public void editEvent(final AnniversaryEvent event) { 
		if (event == null)
			return;
		
		for (int i = 0; i < this.eventList.size(); i++) {
			if (this.eventList.get(i).equals(event))
				this.eventList.set(i, event);		
		}
		
		this.anniversaryList.clear();
		addAnniversariesToList(this.eventList);
	}

	/**
	 * 
	 * @param event Event to be removed
	 */
	public void removeEvent(final AnniversaryEvent event) {
		if (event == null)
			return;
		
		for (int i = 0; i < this.eventList.size(); i++) {
			if (this.eventList.get(i).equals(event))
				this.eventList.remove(i);	
		}
		
		this.anniversaryList.clear();
		addAnniversariesToList(this.eventList);
	}
	
	/**
	 * 
	 * @param event New event, used to be source for new anniversary
	 * @throws DateTimeException
	 */
	private void addNewAnniversaryToList(final AnniversaryEvent event) throws DateTimeException {
		final List<AnniversaryEvent> newEvent = new ArrayList<>();
		newEvent.add(event);
		addAnniversariesToList(newEvent);
	}
	
	/**
	 * This method fills List with eventList content and sort this list, using
	 * natural ordering of events, for which compareTo method from 
	 * <code>victorzh.eventtracker.model.AnniversaryEvent<code> class is used. 
	 * Anniversaries in list are sorted by date in chronological order.
	 * 
	 * Anniversaries, that should be repeated monthly (see isMonthlyRepeatOn
	 * and isYearlyRepeatOn methods in 
	 * <code>victorzh.eventtracker.model.AnniversaryEvent<code>) are added
	 * to list according to its repeaters state. If isMonthlyRepeatOn is true,
	 * anniversary is added to list multiple times, being created for every month
	 * (at appropriated day of month). Otherwise it is added once for year.
	 * 
	 * Place emphasis on fact, that 'anniversary list distance' in List, later
	 * used for filling event panel container in program main window, is set at
	 * 12 months.
	 * 
	 * @see victorzh.eventtracker.model.AnniversaryEvent#isMonthlyRepeatOn()
	 * @see victorzh.eventtracker.model.AnniversaryEvent#isYearlyRepeatOn()
	 * @see victorzh.eventtracker.model.AnniversaryEvent#compareTo(AnniversaryEvent)
	 * 
	 * @param eventList List of eventList, fetched from data storage at startup
	 * 
	 * @throws DateTimeException Exception is thrown when number of day is
	 * tried to be assigned to month, which length in days is less than
	 * mentioned number of day (February 29 on non-leap year, 30th day
	 * on February, April, June, September and November).
	 */

	private List<Anniversary> addAnniversariesToList(final List<AnniversaryEvent> events)
			throws DateTimeException {
		
		final LocalDate currentDate = this.today;
		LocalDate anniversaryDate;
		
		for (AnniversaryEvent event : events) {
			if (event.isMonthlyRepeatOn()) {
				
				//Adding monthly anniversary for current year
				//(from the very beginning of 12-months period till NY occurs)
				for (int month = currentDate.getMonthValue(); month <= DECEMBER; month++) {
					try {
						anniversaryDate = LocalDate.of(
								currentDate.getYear(), 
								month, 
								event.getDate().getDayOfMonth());
					} catch (DateTimeException e) {
						anniversaryDate = LocalDate.of(
								currentDate.getYear(),
								month,
								getLastMonthDay(currentDate.getYear() + 1, month));
						log.warn(
								"Date in event: {}, in anniversary changed to: {}. Event: {}",
								event.getDate(), 
								anniversaryDate,
								event);
					}
								
					if (!anniversaryDate.isBefore(currentDate))
						this.anniversaryList.add(
								new Anniversary(
										event,
										anniversaryDate,
										event.getDescription(),
										event.getPerson(),
										event.isReminderOn(),
										event.isYearlyRepeatOn(),
										event.isMonthlyRepeatOn()));
				}
				
				//Adding monthly anniversary for next year
				//(for period until current month in next year)
				for (int month = 1; month <= currentDate.getMonthValue(); month++) {
					try {
						anniversaryDate = LocalDate.of(
								currentDate.getYear() + 1, 
								month,
								event.getDate().getDayOfMonth());
					} catch (DateTimeException e) {
						anniversaryDate = LocalDate.of(
								currentDate.getYear() + 1, 
								month,
								getLastMonthDay(currentDate.getYear() + 1, month));
						log.warn(
								"Date in event: {}, in anniversary changed to: {}. Event: {}",
								event.getDate(), 
								anniversaryDate,
								event);
					}
					
					this.anniversaryList.add(
							new Anniversary(
									event,
									anniversaryDate,
									event.getDescription(),
									event.getPerson(),
									event.isReminderOn(),
									event.isYearlyRepeatOn(),
									event.isMonthlyRepeatOn()));
				}
			}
			else {
				//Adding yearly anniversary, that is still in future for this period
				//(12 months from current day)
				if ((currentDate.getMonthValue() == event.getDate().getMonthValue()
						&& currentDate.getDayOfMonth() < event.getDate().getDayOfMonth())
						|| ((currentDate.getMonthValue() < event.getDate().getMonthValue()))) {
					
					//Adding event, that will occur before NY
					if (currentDate.getMonthValue() <= DECEMBER) {
						try {
							anniversaryDate = LocalDate.of(
									currentDate.getYear(),
									event.getDate().getMonthValue(),
									event.getDate().getDayOfMonth());
						}
						catch (DateTimeException e){
							anniversaryDate = LocalDate.of(
									currentDate.getYear(),
									event.getDate().getMonthValue(),
									event.getDate().getDayOfMonth() - 1); //for February 29th of non-leap year 
							log.warn(
									"Date in event: {}, in anniversary changed to: {}. Event: {}",
									event.getDate(), 
									anniversaryDate,
									event);
						}
						this.anniversaryList.add(
								new Anniversary(
										event,
										anniversaryDate,
										event.getDescription(),
										event.getPerson(),
										event.isReminderOn(),
										event.isYearlyRepeatOn(),
										event.isMonthlyRepeatOn()));
					}				
					else {
						//Adding yearly anniversary, that will occur after NY
						try {
							anniversaryDate = LocalDate.of(
									currentDate.getYear() + 1,
									event.getDate().getMonthValue(),
									event.getDate().getDayOfMonth());
						}
						catch (DateTimeException e){
							anniversaryDate = LocalDate.of(
									currentDate.getYear() + 1,
									event.getDate().getMonthValue(),
									event.getDate().getDayOfMonth() - 1); //for February 29th of non-leap year
							log.warn(
									"Date in event: {}, in anniversary changed to: {}. Event: {}",
									event.getDate(), 
									anniversaryDate,
									event);
						}
						this.anniversaryList.add(
								new Anniversary(
										event,
										anniversaryDate,
										event.getDescription(),
										event.getPerson(),
										event.isReminderOn(),
										event.isYearlyRepeatOn(),
										event.isMonthlyRepeatOn()));
					}
				}
				else {
					//Adding next yearly anniversary, that is already occurred in this year
					try {
						anniversaryDate = LocalDate.of(
								currentDate.getYear() + 1,
								event.getDate().getMonthValue(),
								event.getDate().getDayOfMonth());
					}
					catch (DateTimeException e) {
						anniversaryDate = LocalDate.of(
								currentDate.getYear() + 1,
								event.getDate().getMonthValue(),
								event.getDate().getDayOfMonth() - 1); //for February 29th of non-leap year
						log.warn(
								"Date in event: {}, in anniversary changed to: {}. Event: {}",
								event.getDate(), 
								anniversaryDate,
								event);
					}
					this.anniversaryList.add(
							new Anniversary(
									event,
									anniversaryDate,
									event.getDescription(),
									event.getPerson(),
									event.isReminderOn(),
									event.isYearlyRepeatOn(),
									event.isMonthlyRepeatOn()));
				}
			}
		}
		
		Collections.sort(anniversaryList);
		return anniversaryList;
	}
	
	/**
	 * 
	 * @param year Year, when anniversary occurs
	 * @param month Month of anniversary
	 * @return Last day of month, when anniversary occurs
	 */
	private int getLastMonthDay(final int year, final int month) {
		int dayValue;
		switch (month) {
		case 1: case 3: case 5: case 7: case 8: case 10: case 12:
			dayValue = 31;
			break;
		case 4: case 6: case 9: case 11:
			dayValue = 30;
			break;
		case 2:
			dayValue = Year.isLeap((long)year) ? 29 : 28;
			break;
		default:
			dayValue = 0; //never happens
			break;
		}
		return dayValue;
	}

}
