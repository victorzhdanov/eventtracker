package victorzh.eventtracker.model;

import java.time.LocalDate;

/**
 * This class is needed for creating repeatable anniversaries.
 * It is fully similar to its superclass, the only difference is
 * in supplementary 'event' field, holding reference to parent event,
 * from witch instances of this class inherit all data.
 * The above reference is needed for edit/remove operations in GUI,
 * allowing user to interact with events, referring to anniversaries
 * in <code>victorzh.eventtracker.swingui.EventPanelContainer</code>.
 * 
 * @see victorzh.eventtracker.model.AnniversaryEvent#AnniversaryEvent(LocalDate, String, Person, boolean, boolean, boolean)
 * 
 * @author Victor Zhdanov
 *
 */
public class Anniversary extends AnniversaryEvent {
	private static final long serialVersionUID = -4296750063666836551L;
	private AnniversaryEvent event;

	/**
	 * Superclass constructor made private to prevent creating instances
	 * of this class with null reference to event, for the reason that 
	 * it will damage edit/remove operations in GUI.
	 */
	
	private Anniversary(
			final LocalDate date, 
			final String description, 
			final Person person, 
			final boolean reminder, 
			final boolean yearlyRepeat,
			final boolean monthlyRepeat) {
		
		super(date, description, person, reminder, yearlyRepeat, monthlyRepeat);
	}
	
	/**
	 * Constructor
	 * 
	 * @param year	Year of the event (for eventDate field)
	 * 
	 * @param month	Month of the event (for eventDate field)
	 * 
	 * @param day	Day of the event (for eventDate field)
	 * 
	 * @param description	Description of the event (any text from user input)
	 * 
	 * @param person	Person, related with the event
	 * 
	 * @param reminder	Reminder state. True, if reminder is on, false if off.
	 * On by default. 
	 * 
	 * @param repeat	Repeater state. Indicates, how often the event will be
	 * reminded about (monthly/yearly)
	 */
	
	public Anniversary(
			final AnniversaryEvent event, 
			final LocalDate date, 
			final String description, 
			final Person person, 
			final boolean reminder, 
			final boolean yearlyRepeat,
			final boolean monthlyRepeat) {
		
		super(date, description, person, reminder, yearlyRepeat, monthlyRepeat);
		this.event = event;
	}

	public AnniversaryEvent getEvent() {
		return this.event;
	}

}
