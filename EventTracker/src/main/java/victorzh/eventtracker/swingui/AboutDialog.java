package victorzh.eventtracker.swingui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import net.miginfocom.swing.MigLayout;
import victorzh.eventtracker.util.WindowPositionAdjuster;

/**
 * This dialog is a part of Swing-based GUI of Event Tracker.
 * It is shown when user click "Help - About" items in main menu. 
 * 
 * @author Victor Zhdanov
 *
 */


@SuppressWarnings("serial")
class AboutDialog extends JDialog {
	
	public AboutDialog(final JFrame owner) {
		
		//Setting dialog properties
		setTitle("About EventTracker");
		setLayout(new MigLayout());
		setResizable(false);
		setModal(true);
		setIconImage(new ImageIcon(getClass().getResource("/icons/MainIcon32.gif")).getImage());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			
		//Adding image
		final JLabel appIconLabel = new JLabel();
		appIconLabel.setIcon(new ImageIcon(getClass().getResource("/icons/MainIcon64.gif")));
		
		//Adding text
		final JTextArea textArea = new JTextArea();
		textArea.append(
				"Event Tracker\n"
				+ "Tracking events and anniversaries\n"
				+ "version 0.0.1-SNAPSHOT\n"
				+ "\u00A9 2016 Victor Zhdanov");
		textArea.setEditable(false);
		textArea.setOpaque(false);
		textArea.setFont(new Font(null, 0, 12));

		//Adding OK button
		final JButton buttonOk = new JButton("OK");
		buttonOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AboutDialog.this.setVisible(false);
			}
		});
		
		final JPanel buttonOkPanel = new JPanel();
		buttonOkPanel.add(buttonOk);
		
		//Adding components to dialog
		add(appIconLabel, "align left, gapright 5, gapbottom 5");
		add(textArea, "wrap, align right");
		add(buttonOkPanel, "span, align center");
		
		//Setting size and location of the dialog window
		pack();
		setLocation(WindowPositionAdjuster.getCenteredPosition(this));
	}
}
