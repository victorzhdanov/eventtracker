package victorzh.eventtracker.swingui;

import java.io.IOException;
import java.nio.file.Path;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import victorzh.eventtracker.model.*;
import victorzh.eventtracker.services.FileLoader;
import victorzh.eventtracker.util.WindowPositionAdjuster;

/**
 * The <code>Controller</code> class provides next operations with events:
 * <ul>
 * <li>add event;</li>
 * <li>get event list;</li>
 * <li>get anniversary set;</li>
 * <li>edit event;</li>
 * <li>remove event.</li>
 * </ul>
 * 
 * @author Victor Zhdanov
 *
 */

public class Controller {
	final Logger log = LoggerFactory.getLogger(Controller.class); 
	
	private final MainFrame mainFrame;
	private final EventCollection eventCollection;
	
	/*
	 * This field is needed for correct EventPanelContainer refreshing, when
	 * editing of the event was cancelled by user in EventDetailsDialog.
	 */
	private AnniversaryEvent[] eventToRemove;
	
	
	/**
	 * Controller constructor.
	 * Initialization of Event Collection (including loading user data from
	 * external source) happens here.
	 * 
	 * @param mainFrame Main window in MVC View.
	 */
	public Controller(final MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		this.eventCollection = new EventCollection();
	}
	
	/**
	 * This method is used only for the case, when 'cancel' button is pressed in
	 * EventDetaildDialog while editing event. 
	 */
	public void clearEventToRemove() {
		this.eventToRemove = null;
	}
	
	/**
	 * This method checks input and rejects an event, that is already added.
	 * Reminders state does not matter for reject.
	 * Reminders are stored in array, where 0 is checked yearly repeat box
	 * and 1 is checked monthly repeat box.
	 * 
	 * @see victorzh.eventtracker.model.AnniversaryEvent#hashCode()
	 */	
	public void saveEvent() {
		final EventDetailsDialog dialog = this.mainFrame.getEventDetailsDialog();
		
		try {
			final LocalDate date = dialog.getDate();
			final String description = dialog.getDescription();
			final Person person = dialog.getPerson();
			final boolean reminder = dialog.getReminderState();
			final boolean[] repeaters = dialog.getRepeaters();
			final boolean yearlyRepeat = repeaters[0];
			final boolean monthlyRepeat = repeaters[1];
			
			final AnniversaryEvent event = new AnniversaryEvent(
					date, 
					description, 
					person, 
					reminder, 
					yearlyRepeat, 
					monthlyRepeat);
			
			this.log.info("New anniversary added: {}", event);
			
			for (AnniversaryEvent e : this.eventCollection.getEventList())
				if (e.equals(event)) {
					JOptionPane.showMessageDialog(
							dialog, 
							"This event already exists", 
							"Save error", 
							JOptionPane.ERROR_MESSAGE);
					
					return;
				}
			
			this.eventCollection.addEvent(event);
			dialog.setVisible(false);		
		}
		catch (DateTimeException e) {
			this.log.warn("Incorrect date selected (leap-year problem), {} caught", e.getClass());
			JOptionPane.showMessageDialog(
					dialog, 
					"You are trying to save event with wrong date:\n"
							+ "leap year problem seems to occure.\n"
							+ "Setting 28.02.YYYY is good solution.");
		}
		finally {
			if (this.eventToRemove != null)
				removeEvent(eventToRemove);
			else
				this.mainFrame.refreshEventPanels();	
		}
	}
	
	
	/**
	 * This method performs group remove for all events, selected in 
	 * the main window.
	 * When 'remove' button in event panel is pressed directly, this method
	 * get an array with one element and removes event[0] properly. In this
	 * case selections does not matter.
	 * Good idea is to deal with this method in the same way as with
	 * Collection.removeAll(...) method, though this parameter is not generic.
	 * 
	 * @param events Array of selected events
	 */	
	public void removeEvent(final AnniversaryEvent[] events) {
		if (events.length > 0)
			for (AnniversaryEvent e : events)
				this.eventCollection.removeEvent(e);
		
		this.mainFrame.refreshEventPanels();
	}
	
	
	/**
	 * 
	 * @param event Event to be edited
	 */	
	public void showEventDialog(final AnniversaryEvent event) {
		final EventDetailsDialog dialog = this.mainFrame.getEventDetailsDialog();
		dialog.setLocation(WindowPositionAdjuster.getCenteredPosition(dialog));
		
		if (event == null) {
			dialog.setFieldValues();
			dialog.setVisible(true);
		}
		else {
			dialog.setFieldValues(event);
			this.eventToRemove = new AnniversaryEvent[]{event};
			dialog.setVisible(true);
		}
	}
	
	
	/**
	 * 
	 * @return List of events from <code>victorzh.eventtracker.model.EventCollection</code>
	 */
	List<Anniversary> getAnniversaryList() {
		return this.eventCollection.getAnniversaryList();
	}
	
	/**
	 * 
	 * @return List of events
	 */
	List<AnniversaryEvent> getEventsList() {
		return this.eventCollection.getEventList();
	}
	
	/**
	 * 
	 * @param path Path to file to load events from
	 */
	void loadEventsFromFile(final Path path) {
		this.eventCollection.fillEventList(path);
		this.mainFrame.refreshEventPanels();
	}
	
	EventCollection getEventCollection() {
		return this.eventCollection;
	}
	
	/**
	 * Saving user data to file as text or as serialized object on program
	 * exit. The concrete form of data can be set up in program preferences.
	 * <p>
	 * While global settings are not implemented, both plain text file and
	 * serialized objects are created/updated on exit. The concrete source
	 * of data, used while program starts, can be selected in 
	 * <code>EventCollection</code> constructor.
	 */
	void saveDataOnExit() {
		try {
			//Saving events as text
			FileLoader.saveEvents(
					this.eventCollection.getEventList(), 
					this.eventCollection.getEventFilePath());
			
			log.info(
					"Events successfully saved to file {} on exit.", 
					this.eventCollection.getEventFilePath());
			
			
			//Saving serialized AnniversaryEvents
			FileLoader.writeSerializedData(
					this.eventCollection.getEventList(),
					this.eventCollection.getSerializedEventsPath());
			
			log.info(
					"Events successfully serialized to file {} on exit.", 
					this.eventCollection.getSerializedEventsPath().toFile());
			
			//Saving anniversaries (for default event base only)
			if (this.eventCollection.getEventFilePath().getFileName().toString().equals(EventCollection.EVENTS_PLAIN)
					|| this.eventCollection.getSerializedEventsPath().getFileName().toString().equals(EventCollection.EVENTS_SERIALIZED)) {
				
				//Saving anniversaries as plain text
				FileLoader.saveAnniversaries(
						this.eventCollection.getAnniversaryList(),
						this.eventCollection.getAnniversaryFilePath());
				
				log.info(
						"Anniversaries successfully saved to file {} on exit.", 
						this.eventCollection.getAnniversaryFilePath());
				
				//Saving serialized anniversaries
				FileLoader.writeSerializedData(
						this.eventCollection.getAnniversaryList(), 
						this.eventCollection.getSerializedAnniversariesPath());
				
				log.info(
						"Anniversaries successfully serialized to file {} on exit.", 
						this.eventCollection.getAnniversaryFilePath());
			}
			System.exit(0);
			
		} catch (IOException e1) {
			log.warn("Problem with saving user data while program exit. {} caught.", e1.getClass());
			int choice = JOptionPane.showConfirmDialog(
					this.mainFrame, 
					"Problem with saving user data.\nYour changes are NOT saved.\nExit anyway?", 
					"Can't save data", 
					JOptionPane.YES_NO_OPTION, 
					JOptionPane.WARNING_MESSAGE);
			
			if (choice == JOptionPane.YES_OPTION) {
				log.warn("User data was not saved correctly. User chosed exit anyway");
				System.exit(0);
			}
		}
	}

}