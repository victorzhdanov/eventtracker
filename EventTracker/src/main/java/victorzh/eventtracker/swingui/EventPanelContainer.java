package victorzh.eventtracker.swingui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import victorzh.eventtracker.model.Anniversary;

/**
 * This class is a container for list of EventPanels (being created for each
 * event). It is needed for performing multiple selection of Event Panels in
 * program main window.
 * 
 * @see victorzh.eventtracker.swingui.MainFrame#selectAllButtonPressed
 * 
 * @author Victor Zhdanov
 *
 */

@SuppressWarnings("serial")
class EventPanelContainer extends JPanel {
	private final List<EventPanel> panels;
	private final Controller controller;
	
	public EventPanelContainer(final Controller controller) {
		this.setLayout(new MigLayout("flowy, gap 1, insets 1"));
		this.panels = new ArrayList<>();
		this.controller = controller;
	}
	
	/**
	 * 
	 * @param anniversaries For each anniversary event panel will be created
	 * @return instance of event panel container, filled with event panels
	 * per each anniversary
	 */
	public EventPanelContainer addPanels(final List<Anniversary> anniversaries) {
		this.removeAll();
		this.panels.clear();
		
		for (Anniversary anniversary : anniversaries) 
			this.panels.add(new EventPanel(this.controller, anniversary));
		
		for (EventPanel panel : panels) 
			this.add(panel, "align left, grow, gap 0");
		
		this.revalidate();
		this.repaint();
		return this;
	}
	
	/**
	 * 
	 * @return List of event panels in this container
	 */
	public List<EventPanel> getPanels() {
		return this.panels;
	}
}
