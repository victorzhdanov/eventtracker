package victorzh.eventtracker.swingui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.Period;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import victorzh.eventtracker.model.*;
import victorzh.eventtracker.services.DateManager;

/**
 * This panel contains information about the event and provides access to
 * Edit and Remove operations, applied to current event.
 * Adding new event creates new instance of this panel (among other actions).
 * <p>
 * The accumulation of panels is used to be added, panel by panel, in program
 * main window, according to panel's anniversaries, sorted in chronological
 * order.
 * 
 * @param event <code>victorzh.eventtracker.model.AnniversaryEvent<code>
 * 
 * @see victorzh.eventtracker.swingui.MainFrame#refreshEventPanel(AnniversaryEvent)
 * @see victorzh.eventtracker.swingui.Controller#addEventAction
 * @see victorzh.eventtracker.swingui.Controller#editEventAction
 * @see victorzh.eventtracker.swingui.Controller#removeEventAction
 * 
 * @author Victor Zhdanov
 *
 */

@SuppressWarnings("serial")
class EventPanel extends JPanel {
	private final Controller controller; 
	private final Anniversary anniversary;
	private final AnniversaryEvent event;
	
	private boolean isSelected = false;

	
	public EventPanel(final Controller controller, final Anniversary anniversary) {
		this.controller = controller;
		this.anniversary = anniversary;
		this.event = anniversary.getEvent();
		setUpPanel();
		
		add(createIconLabel(), "align left, gapafter 5");
		add(createAnniversaryDescriptionLabel(), "align left, gapafter 5");
		add(createNextAnniversaryDetailsLabel(), "align right, push, gapafter 5");
		add(createButtonPanel(), "align right, gapafter 10");
	}
	
	/**
	 * 
	 * @return Event, being source for current event panel
	 */
	public AnniversaryEvent getEvent() {
		return this.event;
	}
	
	/**
	 * 
	 * @return Panel selection state
	 */
	public boolean isSelected() {
		return this.isSelected;
	}
	
	/**
	 * 
	 * @param source Component, from which this method is called
	 * @param clickCount Number of mouse clicks
	 */
	public void fireMouseClick(final Component source, final int clickCount) {
		this.panelListener.mouseClicked(
				new MouseEvent(
						source,
						MouseEvent.MOUSE_CLICKED,
						System.currentTimeMillis(),
						0,
						0, //does not matter
						0, //does not matter
						clickCount,
						false));
	}

	private MouseAdapter panelListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 1) {
				if (EventPanel.this.isSelected == false) {
					EventPanel.this.isSelected = true;
					EventPanel.this.setBorder(BorderFactory.createLoweredBevelBorder());
				} else {
					EventPanel.this.isSelected = false;
					EventPanel.this.setBorder(BorderFactory.createRaisedSoftBevelBorder());
				}
			}
			else if (e.getClickCount() == 2) {			
				EventPanel.this.controller.showEventDialog(EventPanel.this.event);
				EventPanel.this.isSelected = false;
				EventPanel.this.setBorder(BorderFactory.createRaisedSoftBevelBorder());
			}
		}
	};
	
	private void setUpPanel() {
		setBorder(BorderFactory.createRaisedSoftBevelBorder());
		setLayout(new MigLayout("wmin 620"));
		addMouseListener(this.panelListener);
	}
	
	private JLabel createIconLabel() {
		final JLabel iconLabel = new JLabel(new ImageIcon(getClass().getResource("/icons/MainIcon32.gif")));
		return iconLabel;
	}
	
	private JLabel createAnniversaryDescriptionLabel() {
		final String repeater = this.event.isMonthlyRepeatOn() ? "monthly" : "yearly"; 
		final StringBuilder eventDescription = new StringBuilder();
		eventDescription.append("<html>");
		eventDescription.append(this.event.getPerson());
		eventDescription.append("<br>");
		eventDescription.append(this.event.getDescription() + ", " + repeater);
		eventDescription.append("</html>");
		
		return new JLabel(eventDescription.toString());
	}
	
	private JLabel createNextAnniversaryDetailsLabel() {
		final StringBuilder anniversaryDetails = new StringBuilder();
		anniversaryDetails.append("will be ");
		anniversaryDetails.append(DateManager.getNextAge(this.event, this.anniversary));
		anniversaryDetails.append(" on ");
		anniversaryDetails.append(this.anniversary.getDate());
		anniversaryDetails.append(" ");
		
		
		final Period daysBefore = DateManager.getDaysBefore(this.anniversary);
			
		if (daysBefore.isZero()) anniversaryDetails.append("Today");
		else anniversaryDetails.append("in ");
		
		if (daysBefore.getYears() != 0) {
			anniversaryDetails.append(daysBefore.getYears());
			anniversaryDetails.append(" y. ");
		}
		if (daysBefore.getMonths() != 0) {
			anniversaryDetails.append(daysBefore.getMonths());
			anniversaryDetails.append(" m. ");
		}
		if (daysBefore.getDays() != 0) {
			anniversaryDetails.append(daysBefore.getDays());
			anniversaryDetails.append(" d.");
		}

		return new JLabel(anniversaryDetails.toString());
	}
	
	
	private JPanel createButtonPanel() {
		final JPanel buttonPanel = new JPanel();

		final JButton editButton = new JButton("Edit");
		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EventPanel.this.controller.showEventDialog(EventPanel.this.event);
			}
		});
		editButton.setToolTipText("Edit this event");

		final JButton removeButton = new JButton("Remove");
		removeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int userChoice = JOptionPane.showConfirmDialog(
						null, 
						"Event will be removed. Continue?");
				if (userChoice == JOptionPane.YES_OPTION) {
					AnniversaryEvent[] eventArray = {EventPanel.this.event};
					EventPanel.this.controller.removeEvent(eventArray);
				}
				else return;
			}
		});
		removeButton.setToolTipText("Remove this item...");
		
		buttonPanel.add(editButton);
		buttonPanel.add(removeButton);
		
		return buttonPanel;
	}
}
