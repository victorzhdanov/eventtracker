package victorzh.eventtracker.swingui;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.miginfocom.swing.MigLayout;
import victorzh.eventtracker.model.Anniversary;
import victorzh.eventtracker.model.AnniversaryEvent;
import victorzh.eventtracker.services.FileLoader;
import victorzh.eventtracker.util.WindowPositionAdjuster;

/**
 * This frame is the main frame of Event Tracker GUI.
 * 
 * @author Victor Zhdanov
 */

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	private static final int SCROLLBAR_UNIT_INCREMENT = 15; //one event panel per wheel scroll
	
	private final Logger log = LoggerFactory.getLogger(MainFrame.class);
	
	private AboutDialog aboutDialog;
	private final EventDetailsDialog eventDetailsDialog;
	private EventPanelContainer eventPanelContainer;
	private JFileChooser fileChooser;
	private JPanel statusBar;
	private List<JLabel> statusBarLabels;
	private final Controller controller;
	
	private boolean allEventPanelsAreSelected = false;
	
	public MainFrame() {
		setUpFrame();
		createMenuBar();
		this.controller = new Controller(this);
		
		add(createToolBar(), "grow, north");
		add(createEventPanelContainer(), "grow, center, hmin 500, hmax 500, wmin 620");
		add(createStatusBar(), "grow, south");
		
		refreshEventPanels();
		
		this.eventDetailsDialog = new EventDetailsDialog(this, this.controller);
		this.eventDetailsDialog.setVisible(false);
		
		pack();
		setLocation(WindowPositionAdjuster.getCenteredPosition(this));

	}
	
	/**
	 * 
	 * @return Instance of 
	 * <code>victorzh.eventtracker.swingui.EventDetailsDialog</code>
	 */
	public EventDetailsDialog getEventDetailsDialog() {
		return this.eventDetailsDialog;
	}
	
	void refreshEventPanels() {
		this.eventPanelContainer.addPanels(this.controller.getAnniversaryList());
		refreshStatusBar();
	}
	
	private void refreshStatusBar() {
		int eventAmount = this.controller.getEventCollection().getEventListSize();
		int anniversaryAmount = this.controller.getEventCollection().getAnniversaryListSize();
		
		int todayAnniversariesCounter = 0;
		final LocalDate today = LocalDate.now();
		
		for (Anniversary anniversary : this.controller.getEventCollection().getAnniversaryList()) {
			if (today.equals(anniversary.getDate()))
				todayAnniversariesCounter++;
		}
		
		statusBarLabels.get(0).setText(String.valueOf(eventAmount) + " events");
		statusBarLabels.get(1).setText(String.valueOf(anniversaryAmount) + " anniversaries");
		statusBarLabels.get(2).setText(String.valueOf(todayAnniversariesCounter) + " today");
	}

	private JPanel createStatusBar() {
		this.statusBar = new JPanel();
		this.statusBar.setLayout(new MigLayout("align center, fill, ins 0, gap 0"));
		this.statusBar.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		
		final JLabel eventAmount = new JLabel();
		final JLabel anniversaryAmount = new JLabel();
		final JLabel todayAnniversaries = new JLabel();
		
		this.statusBarLabels = new ArrayList<>(3);
		statusBarLabels.add(eventAmount);
		statusBarLabels.add(anniversaryAmount);
		statusBarLabels.add(todayAnniversaries);
		
		for (JLabel label : statusBarLabels) {
			label.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
			this.statusBar.add(label, "grow");
		}
	
		return this.statusBar;
	}
	
	private JScrollPane createEventPanelContainer() {
		this.eventPanelContainer = new EventPanelContainer(this.controller);
		final JScrollPane eventContainerScrollPane = new JScrollPane(this.eventPanelContainer);
		eventContainerScrollPane.getVerticalScrollBar().setUnitIncrement(SCROLLBAR_UNIT_INCREMENT);
		eventContainerScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		return eventContainerScrollPane;
	}

	private void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			log.info("Plugged system L&F: {}", UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e1) {
			log.error("Problem with plugging system L&F occured. {} thrown.", e1.getCause());
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
				log.info("Plugged cross platform L&F: {}", UIManager.getCrossPlatformLookAndFeelClassName());
			}
			catch (Exception e2) {
				log.error("Problem with plugging cross platform L&F occured. {} thrown.", e2.getCause());
			}
		}
	}
	
	private void setUpFrame() {
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Event Tracker");
		setIconImage(new ImageIcon(getClass().getResource("/icons/MainIcon32.gif")).getImage());
		setLayout(new MigLayout("flowy, fill"));
		setLookAndFeel();
	}
	
	private void createMenuBar() {
		
		//Top-level menu items
		final JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		final JMenu mainMenu = new JMenu("Menu");
		final JMenu eventMenu = new JMenu("Event");
		final JMenu helpMenu = new JMenu("Help");
		
		menuBar.add(mainMenu);
		menuBar.add(eventMenu);
		menuBar.add(helpMenu);
		
		mainMenu.setMnemonic('M');
		eventMenu.setMnemonic('E');
		helpMenu.setMnemonic('H');
		
		
		//Main menu items		
		final Action openFile = this.openFileAction;
		final Action saveFile = this.saveFileAction;	
		final Action showSettings = this.showSettingsFrameAction;
		showSettings.setEnabled(false);
		
		mainMenu.add(openFile);
		mainMenu.add(saveFile);
		mainMenu.add(showSettings);	
		mainMenu.addSeparator();
		mainMenu.add(this.exitAction);
		
		//Event menu items
		eventMenu.add(this.addEventButtonPressed);
		eventMenu.add(this.editButtonPressed);
		eventMenu.add(this.removeButtonPressed);
		
		//Help menu items
		helpMenu.add(this.aboutAction);
		
	}
	
	//Adding tool bar	
	private JToolBar createToolBar() {
		final JToolBar toolBar = new JToolBar();

		final JButton addEventButton = new JButton(this.addEventButtonPressed);
		final JButton editEventButton = new JButton(this.editButtonPressed);
		final JButton removeEventButton = new JButton(this.removeButtonPressed);
		final JButton selectAllButton = new JButton(this.selectAllButtonPressed);
		final JButton exitButton = new JButton(this.exitAction);
		
		addEventButton.setToolTipText("Add new event");
		editEventButton.setToolTipText("Edit selected event");
		removeEventButton.setToolTipText("Remove selected event");
		selectAllButton.setToolTipText("Select all events for group operation");
		exitButton.setToolTipText("Close program");
			
		toolBar.add(addEventButton);
		toolBar.add(editEventButton);
		toolBar.add(removeEventButton);
		toolBar.add(selectAllButton);
		toolBar.addSeparator();
		toolBar.add(exitButton);

		return toolBar;
	}


	//Adding frame buttons and menu items actions
	private final Action addEventButtonPressed = new AbstractAction("Add event") {
		@Override
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.controller.showEventDialog(null);
		}
	};
	
	/**
	 * If multiple events are selected, only the first one will be edited.
	 * Other ones are silently ignored.
	 */
	private final Action editButtonPressed = new AbstractAction("Edit event") {
		@Override
		public void actionPerformed(ActionEvent e) {	
			int selected = 0;
			select_is_done:
			for (EventPanel panel : MainFrame.this.eventPanelContainer.getPanels()) {
				if (panel.isSelected()) {
					final AnniversaryEvent event = panel.getEvent();
					selected++;
					MainFrame.this.controller.showEventDialog(event);
					break select_is_done;
				}
			}
			
			if (selected == 0)
				JOptionPane.showMessageDialog(null, "No selection is done. Nothing to edit");
		}
	};
	
	private final Action removeButtonPressed = new AbstractAction("Remove selected") {
		@Override
		public void actionPerformed(ActionEvent e) {
			int userChoice =  JOptionPane.showConfirmDialog(
					MainFrame.this, 
					"All selected events will be removed. Continue?");
			
			if (userChoice == JOptionPane.YES_OPTION) {
				int arraySize = MainFrame.this.controller.getAnniversaryList().size();
				final AnniversaryEvent[] events = new AnniversaryEvent[arraySize];
				
				//Building array that will be passed to Controller.removeEvent method
				int eventsIterator = 0;
				for (EventPanel panel : MainFrame.this.eventPanelContainer.getPanels()) {
					if (panel.isSelected()) {
						events[eventsIterator] = panel.getEvent();
						eventsIterator++; 
					}
				}	
				MainFrame.this.controller.removeEvent(events);
				
				//Reestablishing selection mode
				if (MainFrame.this.allEventPanelsAreSelected == true)
					MainFrame.this.allEventPanelsAreSelected = false;
			}
		}
	};
	
	private final Action selectAllButtonPressed = new AbstractAction("Select all") {
		@Override
		public void actionPerformed(ActionEvent e) {			
			if (MainFrame.this.allEventPanelsAreSelected == false) {
				for (EventPanel panel : MainFrame.this.eventPanelContainer.getPanels()) {
					if (!panel.isSelected()) {
						panel.fireMouseClick(MainFrame.this, 1);
						MainFrame.this.allEventPanelsAreSelected = true;
					}	
				}
			} else {
				for (EventPanel panel : MainFrame.this.eventPanelContainer.getPanels()) {
					if (panel.isSelected()) {
						panel.fireMouseClick(MainFrame.this, 1);
						MainFrame.this.allEventPanelsAreSelected = false;
					}
				}
			}
		}
	};
	
	private final Action exitAction = new AbstractAction("Exit") {
		@Override
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.controller.saveDataOnExit();
		}
	};
	
	private final Action showSettingsFrameAction = new AbstractAction("Settings") {
		@Override
		public void actionPerformed(ActionEvent e) {
			//TODO IN NEXT MAJOR RELEASE Add global program settings and settings window - see TODO.txt
		}
	};
	
	/**
	 * IOException, thrown by 
	 * <code>victorzh.eventtracker.services.FileLoader.loadData</code> method
	 * is caught in
	 * <code>victorzh.eventtracker.model.EventCollection.fillEventList</code>
	 * method, thus no try-catch is needed here.
	 */
	private final Action openFileAction = new AbstractAction("Open file...") {
		@Override
		public void actionPerformed(ActionEvent e) {			
			if (MainFrame.this.fileChooser == null) {
				MainFrame.this.fileChooser = new JFileChooser(new File("."));
				final FileFilter filter = new FileNameExtensionFilter("*.dat files only", "dat");
				MainFrame.this.fileChooser.addChoosableFileFilter(filter);
				MainFrame.this.fileChooser.setAcceptAllFileFilterUsed(false);
			}
			
			MainFrame.this.fileChooser.setDialogTitle("Select file with event base");
				
			int choice = MainFrame.this.fileChooser.showOpenDialog(MainFrame.this);
			
			if (choice == JFileChooser.APPROVE_OPTION) {
				final Path eventFilePath = MainFrame.this.fileChooser.getSelectedFile().toPath();
				MainFrame.this.controller.loadEventsFromFile(eventFilePath);
				MainFrame.this.controller.getEventCollection().setEventFilePath(eventFilePath); //new events will be saved to last opened base
			}
		}
	};
	
	/**
	 * This method calls
	 * <code>victorzh.eventtracker.services.FileLoader.saveData</code>
	 * (in which IOException can be thrown) directly, thus try-catch is used here.
	 */
	private final Action saveFileAction = new AbstractAction("Save as...") {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (MainFrame.this.fileChooser == null)
				MainFrame.this.fileChooser = new JFileChooser(new File("."));
			
			MainFrame.this.fileChooser.setDialogTitle("Save current event base to file");
			
			int choice = MainFrame.this.fileChooser.showSaveDialog(MainFrame.this);
			if (choice == JFileChooser.APPROVE_OPTION) {
				try {
					FileLoader.saveEvents(MainFrame.this.controller.getEventsList(), fileChooser.getSelectedFile().toPath());
					log.info("Events successfully saved to file {}", fileChooser.getSelectedFile());
				} catch (IOException e2) {
					log.warn("Problem with saving user data via 'save as...' menu item {} caught.", e2.getClass());
					int dialogChoice = JOptionPane.showConfirmDialog(
							MainFrame.this, 
							"Problem with saving event base.\nFile is NOT saved.\nExit anyway?",
							"Can't save event base",
							JOptionPane.YES_NO_OPTION, 
							JOptionPane.WARNING_MESSAGE);
					
					if (dialogChoice == JOptionPane.YES_OPTION) {
						log.warn("User data was not saved correctly via 'save as...' menu item. User chosed exit anyway");
						System.exit(ERROR);
					}
				}
			}
		}
	};

	private final Action aboutAction = new AbstractAction("About") {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (MainFrame.this.aboutDialog == null) {
				MainFrame.this.aboutDialog = new AboutDialog(MainFrame.this);
				MainFrame.this.aboutDialog.setVisible(true);
			} else {
				MainFrame.this.aboutDialog.setLocation(
						WindowPositionAdjuster.getCenteredPosition(MainFrame.this.aboutDialog));
				
				MainFrame.this.aboutDialog.setVisible(true);
			}
		}
	};

}
