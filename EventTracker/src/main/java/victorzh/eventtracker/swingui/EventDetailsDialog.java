package victorzh.eventtracker.swingui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

import java.time.DateTimeException;
import java.time.LocalDate;

import victorzh.eventtracker.model.AnniversaryEvent;
import victorzh.eventtracker.model.Person;
import victorzh.eventtracker.util.*;

/**
 * This dialog is a part of Swing-based GUI of Event Tracker.
 * User can add or edit event details through this dialog fields. 
 * 
 * @author Victor Zhdanov
 *
 */

@SuppressWarnings("serial")
public class EventDetailsDialog extends JDialog {
	
	//Panels
	private JLabel headText;
	private JTextField descriptionText;
	private JPanel personPanel;
	private DatePicker datePickerPanel;
	private JPanel repeatBoxPanel;
	private JPanel reminderPanel;
	
	//Person panel content
	private JTextField lastName;
	private JTextField firstName;
	private JTextField middleName;
	
	//Repeaters panel content
	private JCheckBox monthlyBox;
	private JCheckBox yearlyBox;
	
	//Reminder panel content
	private JRadioButton reminderOn;
	private JRadioButton reminderOff;

	private JPanel buttonPanel;
	
	private Controller controller;

	
	public EventDetailsDialog(final MainFrame owner, final Controller controller) {	
		this.controller = controller;
		setUpDialogWindow();
		createPanels();
		addPanels();
		pack();
	}
	
	/**
	 * 
	 * @return Date, formed as LocalDate from <code>DatePicker</code> selection
	 * @throws DateTimeException
	 */
	public LocalDate getDate() throws DateTimeException {
		return this.datePickerPanel.getSelectedDate();
	}
	
	/**
	 * 
	 * @return Description of the event
	 */
	public String getDescription() {
		return this.descriptionText.getText();
	}
	
	/**
	 * 
	 * @return New instance of Person, filled with current person's data
	 */
	public Person getPerson() {
		final String[] personArgs = new String[3];
		personArgs[0] = this.lastName.getText();
		personArgs[1] = this.firstName.getText();
		personArgs[2] = this.middleName.getText();
		
		return new Person(personArgs);
	}
	
	/**
	 * 
	 * @return Reminder state (is tray notification for this event enabled
	 * or not)
	 */
	public boolean getReminderState() {
		return this.reminderOn.isSelected();
	}
	
	/**
	 * 
	 * @return Repeaters state (is this event a monthly event or it is a
	 * yearly event
	 */
	public boolean[] getRepeaters() {
		final boolean[] repeaters = new boolean[2];
		repeaters[0] = this.yearlyBox.isSelected();
		repeaters[1] = this.monthlyBox.isSelected();
		return repeaters;
	}
	
	/**
	 * Fills dialog fields with details of event, passed as parameter to this
	 * overloaded method. 
	 * 
	 * @param e Event, that will be used to fill dialog fields.
	 */
	void setFieldValues(final AnniversaryEvent event) {	
		this.descriptionText.setText(event.getDescription());
		this.firstName.setText(event.getPerson().getFirstName());
		this.lastName.setText(event.getPerson().getLastName());
		this.middleName.setText(event.getPerson().getMiddleName());
		this.datePickerPanel.setDate(event.getDate());
		this.reminderOn.setSelected(event.isReminderOn());
		this.monthlyBox.setSelected(event.isMonthlyRepeatOn());
		this.yearlyBox.setSelected(event.isYearlyRepeatOn());
	}
	
	/**
	 * Overloaded method without parameter makes all fields blank.
	 */
	void setFieldValues() {
		this.descriptionText.setText("");
		this.firstName.setText("");
		this.lastName.setText("");
		this.middleName.setText("");
		this.datePickerPanel.setDate(LocalDate.now());
		this.reminderOn.setSelected(true);
		this.monthlyBox.setSelected(false);
		this.yearlyBox.setSelected(true);
	}

	private void setUpDialogWindow() {
		setTitle("Event details");
		setModal(true);
		setResizable(false);
		setLayout(new MigLayout());
		setIconImage(new ImageIcon(getClass().getResource("/icons/MainIcon32.gif")).getImage());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	private void createPanels() {
		//Head text panel
		this.headText = new JLabel("Enter event details below:");
		this.headText.setFont(new Font(null, Font.BOLD, 14));
		
		//Adding date picker
		this.datePickerPanel = new DatePicker();

		//Adding event description
		this.descriptionText = BorderedComponentFactory.getTitledTextField("Event escription");
		this.descriptionText.setText("");
		this.descriptionText.setColumns(50);
		
		//Adding person panel
		this.personPanel = BorderedComponentFactory.getTitledPanel("Related person");
		
		this.personPanel.setLayout(new MigLayout("wrap, align left"));
		this.lastName = BorderedComponentFactory.getTitledTextField("Last name");
		this.firstName = BorderedComponentFactory.getTitledTextField("First name");
		this.middleName = BorderedComponentFactory.getTitledTextField("Middle name");
		
		final List<JTextField> personName = new ArrayList<>(
				Arrays.asList(
						this.lastName,
						this.firstName,
						this.middleName));
		
		for (JTextField field : personName) {
			field.setText("");
			field.setColumns(25);		
			personPanel.add(field);
		}
		
		//Adding reminder switcher
		this.reminderPanel = BorderedComponentFactory.getTitledPanel("Reminder");
		final ButtonGroup reminderGroup = new ButtonGroup();
		this.reminderOn = new JRadioButton("On", false);
		this.reminderOff = new JRadioButton("Off", true);
		reminderGroup.add(this.reminderOn);
		reminderGroup.add(this.reminderOff);
		this.reminderPanel.add(this.reminderOn);
		this.reminderPanel.add(this.reminderOff);
		
		//Adding repeat period selector
		this.repeatBoxPanel = BorderedComponentFactory.getTitledPanel("Remind me...");
		this.monthlyBox = new JCheckBox("Monthly");
		this.yearlyBox = new JCheckBox("Yearly");
		this.monthlyBox.setSelected(false);
		this.yearlyBox.setSelected(true);
		this.repeatBoxPanel.add(this.monthlyBox);
		this.repeatBoxPanel.add(this.yearlyBox);
		
		//Adding Save button
		this.buttonPanel = new JPanel();
		final JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				EventDetailsDialog.this.controller.saveEvent();
			}
		});
		
		//Adding Cancel button 
		final JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EventDetailsDialog.this.controller.clearEventToRemove();
				setVisible(false);
				EventDetailsDialog.this.setFieldValues();
			}
		});
		
		this.buttonPanel.add(saveButton);
		this.buttonPanel.add(cancelButton);
	}
	
	private void addPanels() {
		add(this.headText, "span, align center");
		add(this.descriptionText, "span, align center");
		add(this.personPanel, "span 1 3, align center");
		add(this.datePickerPanel, "wrap, align center");
		add(this.repeatBoxPanel, "wrap, align center");
		add(this.reminderPanel, "wrap, align center");
		add(this.buttonPanel, "span, align center");	
	}
}	
