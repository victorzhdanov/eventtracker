package victorzh.eventtracker.services;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.LoggerFactory;

import victorzh.eventtracker.model.Anniversary;
import victorzh.eventtracker.model.AnniversaryEvent;
import victorzh.eventtracker.model.Person;

/**
 * This class provides static methods for saving user data to and loading it
 * from file in local file system.
 * 
 * @author Victor Zhdanov
 *
 */

public class FileLoader {
	private static final String LINE_COMMENT_SYMBOL = "#";
	
	/**
	 * Prepares strings with event details and writes it to file.
	 * <p>
	 * Note, that toString method of
	 * <code>victorzh.eventtracker.model.AnniversaryEvent</code>
	 * is not used while strings are prepared and written to file.
	 * 
	 * @param events List of AnniversaryEvent instances
	 * @param path Path to file, in which data will be written
	 * @throws IOException
	 */
	
	public static void saveEvents(final List<AnniversaryEvent> events, final Path path)
			throws IOException {
		
		final Writer writer = Files.newBufferedWriter(path);
		final StringBuilder eventStr = new StringBuilder();
		for (AnniversaryEvent event : events) {
			eventStr.append(event.getDate());
			eventStr.append("|");
			eventStr.append(event.getDescription());
			eventStr.append("|");
			eventStr.append(event.getPerson().getLastName());
			eventStr.append("&");
			eventStr.append(event.getPerson().getFirstName());
			eventStr.append("&");
			eventStr.append(event.getPerson().getMiddleName());
			eventStr.append("|");
			eventStr.append(event.isReminderOn());
			eventStr.append("|");
			eventStr.append(event.isYearlyRepeatOn());
			eventStr.append("|");
			eventStr.append(event.isMonthlyRepeatOn());
			eventStr.append("\n");
			
			writer.write(eventStr.toString());
			
			eventStr.delete(0, eventStr.toString().length());
		}
		
		writer.close();
	}
	
	/**
	 * Method saves anniversaries in separated file for loading in
	 * <code>victorzh.eventtracker.services.Reminder</code>
	 * 
	 * @param anniversaries List of anniversaries
	 * @param path Path to file, in which data will be written
	 * @throws IOException
	 */
	public static void saveAnniversaries(final List<Anniversary> anniversaries, final Path path) 
			throws IOException {
		
		final Writer writer = Files.newBufferedWriter(path);
		final StringBuilder anniversaryStr = new StringBuilder();
		for (Anniversary anniversary : anniversaries) {
			anniversaryStr.append(anniversary.getDate());
			anniversaryStr.append("|");
			anniversaryStr.append(anniversary.getDescription());
			anniversaryStr.append("|");
			anniversaryStr.append(anniversary.getPerson().getLastName());
			anniversaryStr.append("&");
			anniversaryStr.append(anniversary.getPerson().getFirstName());
			anniversaryStr.append("&");
			anniversaryStr.append(anniversary.getPerson().getMiddleName());
			anniversaryStr.append("|");
			anniversaryStr.append(anniversary.isReminderOn());
			anniversaryStr.append("|");
			anniversaryStr.append(anniversary.isYearlyRepeatOn());
			anniversaryStr.append("|");
			anniversaryStr.append(anniversary.isMonthlyRepeatOn());
			anniversaryStr.append("\n");
			
			writer.write(anniversaryStr.toString());
			
			anniversaryStr.delete(0, anniversaryStr.toString().length());
		}
		
		writer.close();
	}
	
	/**
	 * @param path Path to file to load data from
	 * @return List of events, loaded from text file.
	 * @throws IOException
	 */
	public static List<AnniversaryEvent> loadEvents(final Path path) 
			throws IOException {
		
		LocalDate date;
		String description;
		Person person;
		boolean reminder;
		boolean yearlyRepeater;
		boolean monthlyRepeater;
		
		final List<AnniversaryEvent> events = new ArrayList<>();
		
		final Scanner scanner = new Scanner(path);
		
		while (scanner.hasNext()) {
			String line = scanner.nextLine();
			if (!line.startsWith(LINE_COMMENT_SYMBOL)) {
				String[] argLine = line.split("\\|");
				
				date = LocalDate.parse(argLine[0]);
				description = argLine[1];
				
				//Person string is nested in event string, thus it is parsed in a special manner
				String[] personConstructorArgs = {"", "", ""};
				String[] personInputLine = argLine[2].split("\\&");
				
				for (int i = 0; i < personInputLine.length; i++)
					if (!personInputLine[i].equals(personConstructorArgs))
						personConstructorArgs[i] = personInputLine[i];
				
				person = new Person(personConstructorArgs);
				//end of person parsing
				
				reminder = argLine[3].equals("true") ? true : false;
				yearlyRepeater = argLine[4].equals("true") ? true : false;
				monthlyRepeater = argLine[5].equals("true") ? true : false;
				
				events.add(
						new AnniversaryEvent(
								date,
								description,
								person,
								reminder,
								yearlyRepeater,
								monthlyRepeater));
			}
		}
		
		scanner.close();
		
		return events;
	}
	
	/**
	 * 
	 * @param path Path to file, that contain serialized event collection
	 * @return List of events, ready to be loaded in <code>EventCollection</code>
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static List<AnniversaryEvent> readSerializedEvents(final Path path) 
			throws IOException {
		
		final List<AnniversaryEvent> events = new ArrayList<>();
		try {
			final ObjectInputStream in = new ObjectInputStream(new FileInputStream(path.toString()));
			events.addAll((List<AnniversaryEvent>)in.readObject());
			in.close();
			return events;
		} 
		catch (ClassNotFoundException e) {
			LoggerFactory.getLogger(FileLoader.class).warn(
					"Unable to load serialized events. {} caught.",
					e.getClass());
			
			return events; //Program starts with empty window
		}
	}
	
	/**
	 * 
	 * @param events Events or Anniversaries to serialize
	 * @param path Path to file, in which collection of events/anniversaries
	 * will be written
	 *  
	 * @throws IOException
	 */
	public static void writeSerializedData(final List<? extends AnniversaryEvent> events, final Path path)
			throws IOException {
		
		final ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path.toFile()));
		out.writeObject(events);
		out.close();
	}
	
	/**
	 * 
	 * @param path Path to file, supposed to be used as base for user data
	 * @return Is this file valid to be used as base, or not
	 */
	public static boolean isFileValidAsBase(final Path path) {
		if (Files.exists(path) 
				&& Files.isReadable(path)
				&& Files.isWritable(path)
				&& (!Files.isDirectory(path)))
			
			return true;
		
		else 
			return false;
	}

}
