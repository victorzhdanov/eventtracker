package victorzh.eventtracker.services;

/**
 * Fixed modes for different ways of loading user data:
 * <ul>
 * <li>File with plain text in UTF-16;</li>
 * <li>File with serialized collection of events/anniversaries;</li>
 * <li>SQL database.</li>
 * <ul>
 * <p>
 * 
 * @author Victor Zhdanov
 *
 */

public enum DataLoadMode {
	TXT_FILE("plain"),
	SERIALIZED_OBJECT("serialized"),
	SQL_DATABASE("sql");

	private String name;
	
	/**
	 * 
	 * @param name Short name of constant as string
	 */
	DataLoadMode(final String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return Short name of constant as string
	 */
	public String getModeName() {
		return this.name;
	}
}
