package victorzh.eventtracker.services;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import victorzh.eventtracker.EventTracker;
import victorzh.eventtracker.model.Anniversary;
import victorzh.eventtracker.model.Person;


/**
 * Reminder is used to be launched from startup. Every time it starts,
 * list of anniversaries is checked, and if some anniversary is closer
 * to current system date, than predefined amount of days, tray reminder
 * will be activated.
 * 
 * @author Victor Zhdanov
 *
 */

public class Reminder {
	private static final Path ANNIVERSARY_PLAIN_PATH = Paths.get("anniversaries.dat");
	private static final Path ANNIVERSARY_SERIALIZED_PATH = Paths.get("anniversaries.ser");
	
	private final List<Anniversary> anniversaries;
	private int checkedPeriodInDays = 3;
	private DataLoadMode loadMode = DataLoadMode.SERIALIZED_OBJECT;
	
	private static final Logger log = LoggerFactory.getLogger(Reminder.class);
	
	
	public Reminder() {
		this.anniversaries = new ArrayList<>();
		
		String path = System.getProperty("user.home"); //TODO debug
		System.out.println(path); //TODO debug
		
		try {
			if (loadMode == DataLoadMode.TXT_FILE)
				loadPlainTextAnniversaries(ANNIVERSARY_PLAIN_PATH);
			else if (loadMode == DataLoadMode.SERIALIZED_OBJECT) {
				final List<Anniversary> anniversaries = loadSerializedAnniversaries(ANNIVERSARY_SERIALIZED_PATH);
				this.anniversaries.addAll(anniversaries);
			}
				
			searchForNextEvents();
		} 
		catch (IOException e) {
			log.error("Error loading anniversaries from file, {} thrown.", e.getClass());
		}
	}
	
	/**
	 * Method called from Settings, enables one of load modes for user data.
	 * 
	 * @see victorzh.eventtracker.services.DataLoadMode
	 * @param mode One of <code>DataLoadMode</code> constants.
	 */
	public void setDataLoadMode(final DataLoadMode mode) {
		this.loadMode = mode;
	}
	
	/**
	 * Setting amount of days before next anniversary to be reminded about.
	 * 3 days is set by default.
	 * 
	 * @param days Amount of days 
	 */
	public void setCheckedPeriod(final int days) {
		if (days > 0)
			this.checkedPeriodInDays = days;
	}
	
	/**
	 * Anniversaries loader and parser. Method fills list of anniversaries to
	 * be looked over in 
	 * <code>victorzh.eventtracker.services.Reminder.searchForNextEvents</code>
	 * method.
	 * 
	 * @param file File with anniversaries
	 * @throws IOException
	 */
	private void loadPlainTextAnniversaries(final Path path)
			throws IOException { //TODO this exception can not be caught: reminder is loaded as separated class
		
		LocalDate date;
		String description;
		Person person;
		boolean reminder;
		boolean yearlyRepeater;
		boolean monthlyRepeater;
		
		final Scanner scanner = new Scanner(path);
		int anniversaryCounter = 0;
		
		while (scanner.hasNext()) {
			
			String line = scanner.nextLine();
			String[] argLine = line.split("\\|");
			
			date = LocalDate.parse(argLine[0]);
			description = argLine[1];
			
			//Person string is nested in event string, thus it is parsed in a special manner
			String[] personConstructorArgs = {"", "", ""};
			String[] personInputLine = argLine[2].split("\\&");
			
			for (int i = 0; i < personInputLine.length; i++)
				if (!personInputLine[i].equals(personConstructorArgs))
					personConstructorArgs[i] = personInputLine[i];
			
			person = new Person(personConstructorArgs);
			//end of person parsing
			
			reminder = argLine[3].equals("true") ? true : false;
			yearlyRepeater = argLine[4].equals("true") ? true : false;
			monthlyRepeater = argLine[5].equals("true") ? true : false;
			
			if (reminder == true) {
				this.anniversaries.add(
						new Anniversary(
								null, //We don't need references to events for reminder setting
								date,
								description,
								person,
								reminder,
								yearlyRepeater,
								monthlyRepeater));
				anniversaryCounter++;
			}
				
		}
		log.info("{} anniversaries loaded in reminder.", anniversaryCounter);
		scanner.close();
	}
	
	@SuppressWarnings("unchecked")
	private List<Anniversary> loadSerializedAnniversaries(final Path path) {
		final List<Anniversary> anniversaries = new ArrayList<>();
		try {
			final ObjectInputStream in = new ObjectInputStream(new FileInputStream(path.toString()));
			anniversaries.addAll((List<Anniversary>)in.readObject());
			in.close();
			return anniversaries;
		}
		catch (ClassNotFoundException | IOException e) {
			LoggerFactory.getLogger(Reminder.class).warn(
					"Unable to load serialized anniversaries. {} caught.",
					e.getClass());
			return anniversaries; //Empty list
		} 
	}
	
	private void searchForNextEvents() {
		Period period;
		
		for (Anniversary anniversary : anniversaries) {
			period = Period.between(LocalDate.now(), anniversary.getDate());
			log.debug("For anniversary {} period is {} years, {} months, {} days",
					anniversary,	
					period.getYears(),
					period.getMonths(),
					period.getDays());
			
			if (period.getYears() == 0 
					&& period.getMonths() == 0 
					&& period.getDays() <= this.checkedPeriodInDays) {
				
				log.debug("The next anniversary is used to be notified about: {}.\n"
						+ "Period is {} years, {} months, {} days", 
						anniversary,
						period.getYears(),
						period.getMonths(),
						period.getDays());
				
				showTrayNotifier();
			}
		}
	}
	
	
	private void showTrayNotifier() {
		if (!SystemTray.isSupported()) {
			log.warn("System tray is not supported.");
			showDialogReminder();
			return;
		}
		
		final SystemTray tray = SystemTray.getSystemTray();
		
		final TrayIcon trayIcon = new TrayIcon(
				new ImageIcon(
						getClass().getResource("/icons/MainIcon16.gif")).getImage(),
				"Event Tracker");
		
		trayIcon.setImageAutoSize(true);
		trayIcon.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EventTracker.main(null);
			}
		});
		
		final PopupMenu menu = new PopupMenu();
		
		final MenuItem startEventTrackerItem = new MenuItem("Show EventTracker");
		startEventTrackerItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EventTracker.main(null); //TODO closing EventTracker kills tray icon
			}
		});
		
		final MenuItem shutDownNotifierItem = new MenuItem("Shut down notifier");
		shutDownNotifierItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tray.remove(trayIcon);
				log.info("Reminder is shut down by user.");
				System.exit(0);
			}
		});
		
		menu.add(startEventTrackerItem);
		menu.add(shutDownNotifierItem);
		
		trayIcon.setPopupMenu(menu);
		
		try {
			tray.add(trayIcon);
			trayIcon.displayMessage(
					"New anniversary coming soon!",
					"Click baloon or icon to see anniversaries.",
					MessageType.INFO);
			log.debug("Tray icon added, baloon shown.");
		} 
		catch (AWTException e) {
			log.error("Adding icon to system tray failed. {} caught", e.getClass());
		}
	}
	
	/**
	 * In case when tray is not supported, simple dialog will be used instead
	 * of tray notification.
	 */
	private void showDialogReminder() {
		JOptionPane.showMessageDialog(null, "Check anniversaries list!");
		log.warn("Dialog reminder used.");
	}
	
	/**
	 * Main method for stand alone launch from system startup
	 * 
	 * @param args No arguments needed
	 */
	public static void main(String[] args) {
		new Reminder();
		log.info("Reminder started at {}", LocalDateTime.now());
	}
}
