package victorzh.eventtracker.services;

import java.time.LocalDate;
import java.time.Period;

import victorzh.eventtracker.model.Anniversary;
import victorzh.eventtracker.model.AnniversaryEvent;

/**
 * This class does not store any data and contains only static methods,
 * providing periods or month/year amounts between current date and event
 * date, or between event and anniversary dates.
 * Mentioned above periods/values are used mainly in 
 * <code>victorzh.eventtracker.swingui.EventPanel</code>.

 * @author Victor Zhdanov
 *
 */

public class DateManager {
	private static final int MONTHS_IN_YEAR = 12;
	
	public static Period getDaysBefore(final Anniversary anniversary) {
		return Period.between(LocalDate.now(), anniversary.getDate());
	}
	
	public static int getNextAge(final AnniversaryEvent event, final Anniversary anniversary) {
		Period period = Period.between(event.getDate(), anniversary.getDate());	
		if (event.isMonthlyRepeatOn()) {
			final int monthsInYears = period.getYears() * MONTHS_IN_YEAR;
			return monthsInYears + period.getMonths();
		}
		else
			return period.getYears();
	}
}
