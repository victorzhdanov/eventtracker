package victorzh.eventtracker;

import java.awt.EventQueue;

import victorzh.eventtracker.swingui.MainFrame;

/**
 * Entry point class.
 * 
 * Initialization order hint:
 * <ul>
 * <li>Main frame (view in MVC) is initialized in main method.</li>
 * <li>Controller is initialized in Main frame constructor.</li>
 * <li>Event Collection is initialized in Controller constructor.</li>
 * <li>User data loader is called in Event Collection constructor.</li>
 * </ul>
 * 
 * @author Victor Zhdanov
 *
 */


public class EventTracker {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				final MainFrame frame = new MainFrame();
				frame.setVisible(true);
				frame.toFront();
			}
		});
	}
}
