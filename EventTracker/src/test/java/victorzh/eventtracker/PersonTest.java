package victorzh.eventtracker;

import static org.junit.Assert.*;

import org.junit.Test;

import victorzh.eventtracker.model.Person;

public class PersonTest {
	
	Person p1 = new Person(new String[]{"last", "first"});
	Person p2 = new Person(new String[]{"personName"});
	int p1hash = p1.hashCode();
	int p2hash = p2.hashCode();

	@Test
	public void testEqualsObject() {
		assertTrue(p1.equals(p2));
		assertTrue(p2.equals(p1));
	}
	
	@Test
	public void testHashCode() {
		assertEquals(p1.equals(p2) == p2.equals(p1), true);
		assertEquals(p1hash, p2hash);
	}
	
//	@Test
//	public void testPersonString() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testPersonStringString() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testPersonStringStringString() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetFirstName() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetMiddleName() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetLastName() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testToString() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testEqualsObject() {
//		fail("Not yet implemented");
//	}

}
