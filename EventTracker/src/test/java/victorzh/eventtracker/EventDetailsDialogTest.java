package victorzh.eventtracker;

import static org.junit.Assert.*;

import org.junit.Test;

import victorzh.eventtracker.swingui.EventDetailsDialog;

public class EventDetailsDialogTest {

	EventDetailsDialog dialog = new EventDetailsDialog(null, null);
	
	@Test
	public void testGetPerson() {
		assertNotNull(dialog.getPerson());
	}
	
	@Test
	public void testGetRepeaters() {
		assertNotNull(dialog.getRepeaters());
	}

}
