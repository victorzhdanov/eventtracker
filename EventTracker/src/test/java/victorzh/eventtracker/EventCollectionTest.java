package victorzh.eventtracker;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import victorzh.eventtracker.model.Anniversary;
import victorzh.eventtracker.model.AnniversaryEvent;
import victorzh.eventtracker.model.EventCollection;
import victorzh.eventtracker.model.Person;
import victorzh.eventtracker.services.FileLoader;

public class EventCollectionTest {
	List<AnniversaryEvent> events = new ArrayList<>();
	LocalDate startDate = LocalDate.of(2006, 01, 01);
	LocalDate endDate = LocalDate.of(2016, 05, 01);
	final String filePath = "E:/Dev/etbase/dateTestBase.dat";
	final String anniversaryOutputPath = "E:/Dev/etbase/anniversariesTestBase.dat";
	
	int eventIterator = 1;
	
	EventCollection collection;

	@Before
	public void setUp() throws Exception {

		//Fill events list
		long startTime = System.currentTimeMillis();
		while (this.startDate.isBefore(this.endDate)) {
			this.startDate = this.startDate.plusDays(1);
			this.events.add(
					new AnniversaryEvent(
							this.startDate, 
							"description of event #" + this.eventIterator++, 
							new Person(new String[] {"person", "name"}), 
							true, 
							true, 
							true));
		}
		long endTime = System.currentTimeMillis() - startTime;	
		System.out.println("Events filled in " + endTime + " ms. "
				+ "Event collection size is " + this.events.size());

		//Fill anniversary list
		FileLoader.saveEvents(events, Paths.get(this.filePath));
		try {
			startTime = System.currentTimeMillis();
			this.collection = new EventCollection(Paths.get(filePath));
			endTime = System.currentTimeMillis() - startTime;
			System.out.println("Anniversaries filled in " + endTime + " ms. "
					+ "Anniversary collection size is " + this.collection.getAnniversaryListSize());
			
			//Writing anniversaries to file
			try (Writer writer = Files.newBufferedWriter(Paths.get(anniversaryOutputPath))) {
				for (Anniversary anniversary : this.collection.getAnniversaryList())
					writer.write(anniversary.toString());
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
		this.events.clear();
	}

	@Test
	public void testEventCollection() {
		fail("Not yet implemented");
	}

	@Test
	public void testFillEventList() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetEventListSize() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAnniversaryListSize() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddEvent() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetEventList() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAnniversaryList() {
		fail("Not yet implemented");
	}

	@Test
	public void testEditEvent() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemoveEvent() {
		fail("Not yet implemented");
	}
	
	public static void main(String[] args) {
		EventCollectionTest test = new EventCollectionTest();
		try {
			test.setUp();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
