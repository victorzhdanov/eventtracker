package victorzh.eventtracker;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import victorzh.eventtracker.model.AnniversaryEvent;
import victorzh.eventtracker.model.Person;

public class EventTest {

	AnniversaryEvent e = new AnniversaryEvent(LocalDate.of(2013, 6, 21), "description", new Person(new String[]{"personName"}), false, true, false);
	AnniversaryEvent e1 = new AnniversaryEvent(LocalDate.of(2013, 6, 21), "description", new Person(new String[]{"personName"}), false, false, true);
	
	AnniversaryEvent e2 = new AnniversaryEvent(LocalDate.of(1969, 1, 29), "description", new Person(new String[]{"personName"}), false, false, true);
	AnniversaryEvent e3 = new AnniversaryEvent(LocalDate.of(2003, 8, 5), "description", new Person(new String[]{"personName"}), false, false, true);
	
	@Test
	public void testGetDescriptionHistory() {
		assertNotNull(e.getDescriptionHistory());
	}

	@Test
	public void testEqualsObject() {
		assertNotEquals(e, e1);
	}
	
	@Test
	public void testNotEquals() {
		assertNotEquals(e, e1);
	}
}
