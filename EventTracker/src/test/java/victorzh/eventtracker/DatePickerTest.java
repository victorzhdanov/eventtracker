package victorzh.eventtracker;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import victorzh.eventtracker.util.DatePicker;

public class DatePickerTest {
	
	DatePicker datePicker = new DatePicker();
	
	LocalDate today = LocalDate.now();
	int year = today.getYear();
	int month = today.getMonthValue();
	int day = today.getDayOfMonth();

	@Test
	public void testDatePicker() {
		assertNotNull(datePicker);
	}

	@Test
	public void testGetSelectedDate() {
		assertEquals(year, datePicker.getSelectedDate().getYear());
		assertEquals(month, datePicker.getSelectedDate().getMonthValue());
		assertEquals(day, datePicker.getSelectedDate().getDayOfMonth());
	}

}
